﻿using Amazon;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using System;
using System.Threading.Tasks;

namespace awsclientconnectiontest
{
    class Program
    {
        static async Task Main()
        {

            // Source: https://github.com/awsdocs/aws-doc-sdk-examples/blob/main/dotnetv3/STS/AssumeRole/AssumeRoleExample/AssumeRole.cs

            Console.Write("Enter MFA Code: ");
            string mfaTOTP = Console.ReadLine();

            // string sourceProfile = "default";
            string mfaArn = "arn:aws:iam::590396721567:mfa/timmp";
            string roleArn = "arn:aws:iam::100677156151:role/s3_manager";
            // string roleArn = "arn:aws:iam::100677156151:role/reporter";

            var client = new Amazon.SecurityToken.AmazonSecurityTokenServiceClient(RegionEndpoint.USEast2);

            // Get and display the information about the identity of the default user.
            var callerIdRequest = new GetCallerIdentityRequest();
            var caller = await client.GetCallerIdentityAsync(callerIdRequest);
            Console.WriteLine($"Original Caller: {caller.Arn}");

            // Create the request to use with the AssumeRoleAsync call.
            var assumeRoleReq = new AssumeRoleRequest() {
                DurationSeconds = 1600,
                RoleSessionName = "Session1",
                RoleArn = roleArn,
                SerialNumber = mfaArn,
                TokenCode = mfaTOTP
            };

            var assumeRoleRes = await client.AssumeRoleAsync(assumeRoleReq);

            // Now create a new client based on the credentials of the caller assuming the role.
            var client2 = new AmazonSecurityTokenServiceClient(credentials: assumeRoleRes.Credentials);

            // Get and display information about the caller that has assumed the defined role.
            var caller2 = await client2.GetCallerIdentityAsync(callerIdRequest);
            Console.WriteLine($"AssumedRole Caller: {caller2.Arn}");
        }
    }
}
